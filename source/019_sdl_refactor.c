#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#if defined(__linux__)
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#elif defined(_WIN32)
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#include "SDL2/SDL_mixer.h"
#else
#error Not supported!
#endif

typedef char Bool; // 1 : true, 0 : false
#define true  1
#define false 0

typedef struct {
    double x;
    double y;
} Vector2;

typedef struct {
    unsigned int rectangleWidth;
    unsigned int rectangleHeight;
    unsigned int rectangleColorRed;
    unsigned int rectangleColorGreen;
    unsigned int rectangleColorBlue;
    double rectangleX;
    double rectangleY;
    Vector2 rectangleMovementDirection;
    double rectangleMovementSpeed;
} Paddle;

typedef struct {
    double ballX;
    double ballY;
    Vector2 ballMovementDirection;
    double ballMovementSpeed;
    unsigned int ballWidth;
    unsigned int ballHeight;
    SDL_Texture* pBallTexture;
} Ball;

typedef struct {
    unsigned int fieldWidth;
    unsigned int fieldHeight;
    unsigned int backgroundColorRed;
    unsigned int backgroundColorGreen;
    unsigned int backgroundColorBlue;
} Field;

typedef struct {
    unsigned int scorePaddle1;
    unsigned int scorePaddle2;
} Match;

void normalize_vector(Vector2* pVector) {
    double length = sqrt(pVector->x * pVector->x + pVector->y * pVector->y);
    if (length != 0.0) {
        pVector->x /= length;
        pVector->y /= length;
    }
}

// Axis-Aligned Bounding Box (AABB)
Bool check_collision(double x1, double y1, unsigned int width1, unsigned int height1,
                     double x2, double y2, unsigned int width2, unsigned int height2)
{
    return ((x1 < x2 + width2) &&
            (x1 + width1 > x2) &&
            (y1 < y2 + height2) &&
            (y1 + height1 > y2));
}

void reset_ball_position(Ball* pBall, Field field) {
    pBall->ballX = 0.5 * (field.fieldWidth - pBall->ballWidth);
    pBall->ballY = 0.5 * (field.fieldHeight - pBall->ballHeight);
}

void update_paddle(Paddle* pPaddle, double deltaTimeSeconds) {
    normalize_vector(&pPaddle->rectangleMovementDirection);
    pPaddle->rectangleX += pPaddle->rectangleMovementDirection.x * pPaddle->rectangleMovementSpeed * deltaTimeSeconds;
    pPaddle->rectangleY += pPaddle->rectangleMovementDirection.y * pPaddle->rectangleMovementSpeed * deltaTimeSeconds;

    pPaddle->rectangleMovementDirection.x = 0.0;
    pPaddle->rectangleMovementDirection.y = 0.0;
}

Bool check_paddle_ball_collision(Ball ball,
                                 Paddle paddle) {
    return check_collision(ball.ballX, ball.ballY, ball.ballWidth, ball.ballHeight,
                           paddle.rectangleX, paddle.rectangleY, paddle.rectangleWidth, paddle.rectangleHeight);
}

void draw_paddle(SDL_Renderer* pRenderer, Paddle paddle) {
    SDL_Rect rectangle = {};
    rectangle.x = paddle.rectangleX;
    rectangle.y = paddle.rectangleY;
    rectangle.w = paddle.rectangleWidth;
    rectangle.h = paddle.rectangleHeight;
    // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
    SDL_SetRenderDrawColor(pRenderer, paddle.rectangleColorRed, paddle.rectangleColorGreen, paddle.rectangleColorBlue, 255u);
    // <https://wiki.libsdl.org/SDL_RenderDrawRect>
    /* SDL_RenderDrawRect(pRenderer, &rectangle); */
    // <https://wiki.libsdl.org/SDL_RenderFillRect>
    SDL_RenderFillRect(pRenderer, &rectangle);
}

int main(int argc, char* argv[]) {
    // <https://wiki.libsdl.org/SDL_Init>
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        return EXIT_FAILURE;
    }

    // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC8>
    if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF) !=
        (IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF))
    {
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_8.html>
    if (TTF_Init() != 0) {
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_11.html>
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) == -1) {
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
        TTF_Quit();
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    const char title[] = "Refatoração";
    unsigned int width = 1280u;
    unsigned int height = 720u;
    SDL_Window* pSDLWindow;
    // <https://wiki.libsdl.org/SDL_CreateWindow>
    pSDLWindow = SDL_CreateWindow(title,
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  width,
                                  height,
                                  SDL_WINDOW_SHOWN);
    if (pSDLWindow == NULL) {
        // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_12.html#SEC12>
        Mix_CloseAudio();
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
        TTF_Quit();
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    SDL_Renderer* pRenderer = SDL_CreateRenderer(pSDLWindow,
                                                 -1, // Initialize with the first suitable renderer.
                                                 SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (pRenderer == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(pSDLWindow);
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    Field field = {};
    field.fieldWidth = width;
    field.fieldHeight = height;
    // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
    SDL_SetRenderDrawColor(pRenderer, field.backgroundColorRed, field.backgroundColorGreen, field.backgroundColorBlue, 255u);

    Paddle paddle1 = {};
    paddle1.rectangleWidth = width / 40;
    paddle1.rectangleHeight = height / 6;
    paddle1.rectangleColorRed = 255u;
    paddle1.rectangleColorGreen = 255u;
    paddle1.rectangleColorBlue = 255u;
    paddle1.rectangleX = 0.0;
    paddle1.rectangleY = 0.0;
    // paddle1.rectangleMovementDirection = {};
    paddle1.rectangleMovementSpeed = 400.0;

    Paddle paddle2 = {};
    paddle2.rectangleWidth = width / 40;
    paddle2.rectangleHeight = height / 6;
    paddle2.rectangleColorRed = 255u;
    paddle2.rectangleColorGreen = 255u;
    paddle2.rectangleColorBlue = 255u;
    paddle2.rectangleX = width - paddle2.rectangleWidth;
    paddle2.rectangleY = height - paddle2.rectangleHeight;
    // paddle2.rectangleMovementDirection = {};
    paddle2.rectangleMovementSpeed = 400.0;

    Match match = {};

    Ball ball = {};
    // TODO Reference?
    ball.pBallTexture = IMG_LoadTexture(pRenderer, "assets/circle.png");
    if (ball.pBallTexture == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyRenderer>
        SDL_DestroyRenderer(pRenderer);
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(pSDLWindow);
        // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_12.html#SEC12>
        Mix_CloseAudio();
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
        TTF_Quit();
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }
    // <https://wiki.libsdl.org/SDL_QueryTexture>
    SDL_QueryTexture(ball.pBallTexture, NULL, NULL, (int*) &ball.ballWidth, (int*) &ball.ballHeight);
    ball.ballMovementDirection.x = -1.0;
    ball.ballMovementDirection.y = -1.0;
    normalize_vector(&ball.ballMovementDirection);
    ball.ballMovementSpeed = 250.0;
    reset_ball_position(&ball, field);

    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_14.html>
    TTF_Font* pFont = TTF_OpenFont("assets/DejaVuSans.ttf", 48);
    if (pFont == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyTexture>
        SDL_DestroyTexture(ball.pBallTexture);
        // <https://wiki.libsdl.org/SDL_DestroyRenderer>
        SDL_DestroyRenderer(pRenderer);
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(pSDLWindow);
        // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_12.html#SEC12>
        Mix_CloseAudio();
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
        TTF_Quit();
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    Mix_Chunk* pSoundEffect = Mix_LoadWAV("assets/blip.wav");
    if (pSoundEffect == NULL) {
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_18.html>
        TTF_CloseFont(pFont);
        // <https://wiki.libsdl.org/SDL_DestroyTexture>
        SDL_DestroyTexture(ball.pBallTexture);
        // <https://wiki.libsdl.org/SDL_DestroyRenderer>
        SDL_DestroyRenderer(pRenderer);
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(pSDLWindow);
        // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_12.html#SEC12>
        Mix_CloseAudio();
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
        TTF_Quit();
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    Uint64 lastTime = 0;
    // <https://wiki.libsdl.org/SDL_GetPerformanceCounter>
    Uint64 currentTime = SDL_GetPerformanceCounter();

    Bool bRunning = true;
    while (bRunning) {
        // <https://wiki.libsdl.org/SDL_Event>
        SDL_Event event;
        // <https://wiki.libsdl.org/SDL_PollEvent>
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT: {
                    bRunning = false;

                    break;
                }

                case SDL_KEYDOWN:
                case SDL_KEYUP: {
                    Bool bPressed = (event.key.type == SDL_KEYDOWN);
                    if (bPressed) {

                        switch (event.key.keysym.sym) {
                            case SDLK_r: {
                                ++field.backgroundColorRed;

                                break;
                            }

                            case SDLK_g: {
                                ++field.backgroundColorGreen;

                                break;
                            }

                            case SDLK_b: {
                                ++field.backgroundColorBlue;

                                break;
                            }

                            case SDLK_w: {
                                paddle1.rectangleMovementDirection.y -= 1.0;

                                break;
                            }

                            case SDLK_s: {
                                paddle1.rectangleMovementDirection.y += 1.0;

                                break;
                            }

                            case SDLK_UP: {
                                paddle2.rectangleMovementDirection.y -= 1.0;

                                break;
                            }

                            case SDLK_DOWN: {
                                paddle2.rectangleMovementDirection.y += 1.0;

                                break;
                            }

                            case SDLK_ESCAPE: {
                                bRunning = false;

                                break;
                            }

                            default: {
                                break;
                            }
                        }
                    }
                }

                default: {
                    break;
                }
            }
        }

        lastTime = currentTime;
        // <https://wiki.libsdl.org/SDL_GetPerformanceCounter>
        currentTime = SDL_GetPerformanceCounter();
        // <https://wiki.libsdl.org/SDL_GetPerformanceFrequency>
        double deltaTimeSeconds = (currentTime - lastTime) / (double) SDL_GetPerformanceFrequency();
        // double deltaTimeMilliseconds = (currentTime - lastTime) * 1000u / (double) SDL_GetPerformanceFrequency();

        update_paddle(&paddle1, deltaTimeSeconds);
        update_paddle(&paddle2, deltaTimeSeconds);

        ball.ballX += ball.ballMovementDirection.x * ball.ballMovementSpeed * deltaTimeSeconds;
        if (ball.ballX < 0.0) {
            ++match.scorePaddle2;
            reset_ball_position(&ball, field);
        } else if (ball.ballX > field.fieldWidth) {
            ++match.scorePaddle1;
            reset_ball_position(&ball, field);
        }

        ball.ballY += ball.ballMovementDirection.y * ball.ballMovementSpeed * deltaTimeSeconds;
        if (ball.ballY < 0.0) {
            // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_28.html>
            Mix_PlayChannel(-1, pSoundEffect, 0);

            ball.ballY = 0.0;
            ball.ballMovementDirection.y *= -1.0;
        } else if ((ball.ballY + ball.ballHeight) > field.fieldHeight) {
            // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_28.html>
            Mix_PlayChannel(-1, pSoundEffect, 0);

            ball.ballY = field.fieldHeight - ball.ballHeight;
            ball.ballMovementDirection.y *= -1.0;
        }

        if (check_paddle_ball_collision(ball, paddle1)) {
            // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_28.html>
            Mix_PlayChannel(-1, pSoundEffect, 0);
            ball.ballMovementDirection.x *= -1;
        } else if (check_paddle_ball_collision(ball, paddle2)) {
            // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_28.html>
            Mix_PlayChannel(-1, pSoundEffect, 0);
            ball.ballMovementDirection.x *= -1;
        }

        // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
        SDL_SetRenderDrawColor(pRenderer, field.backgroundColorRed, field.backgroundColorGreen, field.backgroundColorBlue, 255u);

        // <https://wiki.libsdl.org/SDL_RenderClear>
        SDL_RenderClear(pRenderer);

        draw_paddle(pRenderer, paddle1);
        draw_paddle(pRenderer, paddle2);

        SDL_Rect rectangle = {};
        rectangle.x = ball.ballX;
        rectangle.y = ball.ballY;
        rectangle.w = ball.ballWidth;
        rectangle.h = ball.ballHeight;
        // <https://wiki.libsdl.org/SDL_RenderCopy>
        SDL_RenderCopy(pRenderer, ball.pBallTexture, NULL, &rectangle);

        char text[256];
        sprintf(text, "Placar: %u x %u", match.scorePaddle1, match.scorePaddle2);
        SDL_Color textColor;
        textColor.r = 255;
        textColor.g = 255;
        textColor.b = 255;
        textColor.a = 255;
        // <https://www.libsdl.org/projects/docs/SDL_ttf/SDL_ttf_44.html>
        SDL_Surface* pSurface = TTF_RenderUTF8_Blended_Wrapped(pFont,
                                                               text,
                                                               textColor,
                                                               width);
        // <https://wiki.libsdl.org/SDL_CreateTextureFromSurface>
        SDL_Texture* pTextTexture = SDL_CreateTextureFromSurface(pRenderer, pSurface);
        // <https://wiki.libsdl.org/SDL_FreeSurface>
        SDL_FreeSurface(pSurface);
        // <https://wiki.libsdl.org/SDL_QueryTexture>
        SDL_QueryTexture(pTextTexture, NULL, NULL, &rectangle.w, &rectangle.h);
        rectangle.x = 0.5 * (width - rectangle.w);
        rectangle.y = 0;
        // <https://wiki.libsdl.org/SDL_RenderCopy>
        SDL_RenderCopy(pRenderer, pTextTexture, NULL, &rectangle);
        // <https://wiki.libsdl.org/SDL_DestroyTexture>
        SDL_DestroyTexture(pTextTexture);

        // <https://wiki.libsdl.org/SDL_RenderPresent>
        SDL_RenderPresent(pRenderer);

        // <https://wiki.libsdl.org/SDL_Delay>
        // Time in milliseconds.
        SDL_Delay(1000u / 60u);
    }

    // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_24.html>
    Mix_FreeChunk(pSoundEffect);

    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_18.html>
    TTF_CloseFont(pFont);

    // <https://wiki.libsdl.org/SDL_DestroyTexture>
    SDL_DestroyTexture(ball.pBallTexture);

    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyRenderer(pRenderer);
    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyWindow(pSDLWindow);

    // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_12.html#SEC12>
    Mix_CloseAudio();
    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
    TTF_Quit();
    // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
    IMG_Quit();
    // <https://wiki.libsdl.org/SDL_Quit>
    SDL_Quit();

    return EXIT_SUCCESS;
}
