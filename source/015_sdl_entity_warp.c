#include <stdlib.h>
#include <math.h>

#if defined(__linux__)
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#elif defined(_WIN32)
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#else
#error Not supported!
#endif

typedef char Bool; // 1 : true, 0 : false
#define true  1
#define false 0

typedef struct {
    double x;
    double y;
} Vector2;

void normalize_vector(Vector2* pVector) {
    double length = sqrt(pVector->x * pVector->x + pVector->y * pVector->y);
    if (length != 0.0) {
        pVector->x /= length;
        pVector->y /= length;
    }
}

// Axis-Aligned Bounding Box (AABB)
Bool check_collision(double x1, double y1, unsigned int width1, unsigned int height1,
                     double x2, double y2, unsigned int width2, unsigned int height2)
{
    return ((x1 < x2 + width2) &&
            (x1 + width1 > x2) &&
            (y1 < y2 + height2) &&
            (y1 + height1 > y2));
}

void reset_ball_position(double* pBallX, double* pBallY, unsigned int ballWidth, unsigned int ballHeight,
                         unsigned int fieldWidth, unsigned int fieldHeight) {
    *pBallX = 0.5 * (fieldWidth - ballWidth);
    *pBallY = 0.5 * (fieldHeight - ballHeight);
}

int main(int argc, char* argv[]) {
    // <https://wiki.libsdl.org/SDL_Init>
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        return EXIT_FAILURE;
    }

    // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC8>
    if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF) !=
        (IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF))
    {
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    const char title[] = "Mudança de posição de entidade";
    unsigned int width = 1280u;
    unsigned int height = 720u;
    SDL_Window* pSDLWindow;
    // <https://wiki.libsdl.org/SDL_CreateWindow>
    pSDLWindow = SDL_CreateWindow(title,
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  width,
                                  height,
                                  SDL_WINDOW_SHOWN);
    if (pSDLWindow == NULL) {
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    SDL_Renderer* pRenderer = SDL_CreateRenderer(pSDLWindow,
                                                 -1, // Initialize with the first suitable renderer.
                                                 SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (pRenderer == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(pSDLWindow);
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    unsigned int backgroundColorRed = 0u;
    unsigned int backgroundColorGreen = 0u;
    unsigned int backgroundColorBlue = 0u;
    // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
    SDL_SetRenderDrawColor(pRenderer, backgroundColorRed, backgroundColorGreen, backgroundColorBlue, 255u);

    unsigned int rectangleWidth = width / 40;
    unsigned int rectangleHeight = height / 6;
    unsigned int rectangleColorRed = 255u;
    unsigned int rectangleColorGreen = 255u;
    unsigned int rectangleColorBlue = 255u;
    double rectangleX = 0.0;
    double rectangleY = 0.0;
    Vector2 rectangleMovementDirection = {};
    double rectangleMovementSpeed = 400.0;

    // TODO Reference?
    SDL_Texture* pBallTexture = IMG_LoadTexture(pRenderer, "assets/circle.png");
    if (pBallTexture == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyRenderer>
        SDL_DestroyRenderer(pRenderer);
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(pSDLWindow);
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }
    int textureWidth;
    int textureHeight;
    // <https://wiki.libsdl.org/SDL_QueryTexture>
    SDL_QueryTexture(pBallTexture, NULL, NULL, &textureWidth, &textureHeight);

    double ballX;
    double ballY;
    reset_ball_position(&ballX, &ballY, textureWidth, textureHeight, width, height);
    Vector2 ballMovementDirection = {};
    ballMovementDirection.x = -1.0;
    ballMovementDirection.y = -1.0;
    normalize_vector(&ballMovementDirection);
    double ballMovementSpeed = 250.0;

    Uint64 lastTime = 0;
    // <https://wiki.libsdl.org/SDL_GetPerformanceCounter>
    Uint64 currentTime = SDL_GetPerformanceCounter();

    Bool bRunning = true;
    while (bRunning) {
        // <https://wiki.libsdl.org/SDL_Event>
        SDL_Event event;
        // <https://wiki.libsdl.org/SDL_PollEvent>
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT: {
                    bRunning = false;

                    break;
                }

                case SDL_KEYDOWN:
                case SDL_KEYUP: {
                    Bool bPressed = (event.key.type == SDL_KEYDOWN);
                    if (bPressed) {

                        switch (event.key.keysym.sym) {
                            case SDLK_r: {
                                ++backgroundColorRed;

                                break;
                            }

                            case SDLK_g: {
                                ++backgroundColorGreen;

                                break;
                            }

                            case SDLK_b: {
                                ++backgroundColorBlue;

                                break;
                            }

                            case SDLK_a:
                            case SDLK_LEFT: {
                                rectangleMovementDirection.x -= 1.0;

                                break;
                            }

                            case SDLK_d:
                            case SDLK_RIGHT: {
                                rectangleMovementDirection.x += 1.0;

                                break;
                            }

                            case SDLK_w:
                            case SDLK_UP: {
                                rectangleMovementDirection.y -= 1.0;

                                break;
                            }

                            case SDLK_s:
                            case SDLK_DOWN: {
                                rectangleMovementDirection.y += 1.0;

                                break;
                            }

                            case SDLK_ESCAPE: {
                                bRunning = false;

                                break;
                            }

                            default: {
                                break;
                            }
                        }
                    }
                }

                default: {
                    break;
                }
            }
        }

        lastTime = currentTime;
        // <https://wiki.libsdl.org/SDL_GetPerformanceCounter>
        currentTime = SDL_GetPerformanceCounter();
        // <https://wiki.libsdl.org/SDL_GetPerformanceFrequency>
        double deltaTimeSeconds = (currentTime - lastTime) / (double) SDL_GetPerformanceFrequency();
        // double deltaTimeMilliseconds = (currentTime - lastTime) * 1000u / (double) SDL_GetPerformanceFrequency();

        normalize_vector(&rectangleMovementDirection);
        rectangleX += rectangleMovementDirection.x * rectangleMovementSpeed * deltaTimeSeconds;
        rectangleY += rectangleMovementDirection.y * rectangleMovementSpeed * deltaTimeSeconds;

        rectangleMovementDirection.x = 0.0;
        rectangleMovementDirection.y = 0.0;

        ballX += ballMovementDirection.x * ballMovementSpeed * deltaTimeSeconds;
        if (ballX < 0.0) {
            reset_ball_position(&ballX, &ballY, textureWidth, textureHeight, width, height);
        } else if (ballX > width) {
            reset_ball_position(&ballX, &ballY, textureWidth, textureHeight, width, height);
        }

        ballY += ballMovementDirection.y * ballMovementSpeed * deltaTimeSeconds;
        if (ballY < 0.0) {
            ballY = 0.0;
            ballMovementDirection.y *= -1.0;
        } else if ((ballY + textureHeight) > height) {
            ballY = height - textureHeight;
            ballMovementDirection.y *= -1.0;
        }

        if (check_collision(ballX, ballY, textureWidth, textureHeight,
                        rectangleX, rectangleY, rectangleWidth, rectangleHeight)) {
            ballMovementDirection.x *= -1;
        }

        // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
        SDL_SetRenderDrawColor(pRenderer, backgroundColorRed, backgroundColorGreen, backgroundColorBlue, 255u);

        // <https://wiki.libsdl.org/SDL_RenderClear>
        SDL_RenderClear(pRenderer);

        SDL_Rect rectangle = {};
        rectangle.x = rectangleX;
        rectangle.y = rectangleY;
        rectangle.w = rectangleWidth;
        rectangle.h = rectangleHeight;
        SDL_SetRenderDrawColor(pRenderer, rectangleColorRed, rectangleColorGreen, rectangleColorBlue, 255u);
        // <https://wiki.libsdl.org/SDL_RenderDrawRect>
        /* SDL_RenderDrawRect(pRenderer, &rectangle); */
        // <https://wiki.libsdl.org/SDL_RenderFillRect>
        SDL_RenderFillRect(pRenderer, &rectangle);

        rectangle.x = ballX;
        rectangle.y = ballY;
        rectangle.w = textureWidth;
        rectangle.h = textureHeight;
        // <https://wiki.libsdl.org/SDL_RenderCopy>
        SDL_RenderCopy(pRenderer, pBallTexture, NULL, &rectangle);

        // <https://wiki.libsdl.org/SDL_RenderPresent>
        SDL_RenderPresent(pRenderer);

        // <https://wiki.libsdl.org/SDL_Delay>
        // Time in milliseconds.
        SDL_Delay(1000u / 60u);
    }

    // <https://wiki.libsdl.org/SDL_DestroyTexture>
    SDL_DestroyTexture(pBallTexture);

    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyRenderer(pRenderer);
    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyWindow(pSDLWindow);
    // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
    IMG_Quit();
    // <https://wiki.libsdl.org/SDL_Quit>
    SDL_Quit();

    return EXIT_SUCCESS;
}
