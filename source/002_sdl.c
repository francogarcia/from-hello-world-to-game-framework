#include <stdlib.h>

#if defined(__linux__)
    #include <SDL2/SDL.h>
#elif defined(_WIN32)
    #include "SDL2/SDL.h"
#else
    #error Not supported!
#endif

int main(int argc, char* argv[]) {
    // <https://wiki.libsdl.org/SDL_Init>
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        return EXIT_FAILURE;
    }

    // <https://wiki.libsdl.org/SDL_Quit>
    SDL_Quit();

    return EXIT_SUCCESS;
}
