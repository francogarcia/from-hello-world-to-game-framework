#include <stdlib.h>

#if defined(__linux__)
    #include <SDL2/SDL.h>
#elif defined(_WIN32)
    #include "SDL2/SDL.h"
#else
    #error Not supported!
#endif

int main(int argc, char* argv[]) {
    // <https://wiki.libsdl.org/SDL_Init>
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        return EXIT_FAILURE;
    }

    const char title[] = "Minha primeira janela durou alguns segundos";
    unsigned int width = 1280u;
    unsigned int height = 720u;
    SDL_Window* pSDLWindow;
    // <https://wiki.libsdl.org/SDL_CreateWindow>
    pSDLWindow = SDL_CreateWindow(title,
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  width,
                                  height,
                                  SDL_WINDOW_SHOWN);
    if (pSDLWindow == NULL) {
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    // <https://wiki.libsdl.org/SDL_Delay>
    // Time in milliseconds.
    SDL_Delay(5u * 1000u);

    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyWindow(pSDLWindow);
    // <https://wiki.libsdl.org/SDL_Quit>
    SDL_Quit();

    return EXIT_SUCCESS;
}
