#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#if defined(__linux__)
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#elif defined(_WIN32)
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#include "SDL2/SDL_mixer.h"
#else
#error Not supported!
#endif

// TODO Custom data types.
typedef char Bool; // 1 : true, 0 : false
#define true  1
#define false 0

typedef struct {
    double x;
    double y;
} Vector2;

typedef struct {
    unsigned int rectangleWidth;
    unsigned int rectangleHeight;
    unsigned int rectangleColorRed;
    unsigned int rectangleColorGreen;
    unsigned int rectangleColorBlue;
    double rectangleX;
    double rectangleY;
    Vector2 rectangleMovementDirection;
    double rectangleMovementSpeed;
} Paddle;

typedef struct {
    double ballX;
    double ballY;
    Vector2 ballMovementDirection;
    double ballMovementSpeed;
    unsigned int ballWidth;
    unsigned int ballHeight;
    SDL_Texture* pBallTexture;
} Ball;

typedef struct {
    unsigned int fieldWidth;
    unsigned int fieldHeight;
    unsigned int backgroundColorRed;
    unsigned int backgroundColorGreen;
    unsigned int backgroundColorBlue;
} Field;

typedef struct {
    unsigned int scorePaddle1;
    unsigned int scorePaddle2;
} Match;

// TODO Bitwise flags.
typedef struct {
    Bool bBallFieldCollided;
    Bool bBallPaddleCollided;
    Bool bPaddle1Scored;
    Bool bPaddle2Scored;
} Event;

// TODO Controller support.
// TODO Bitwise flags.
typedef struct {
    Bool bChangeColorRed;
    Bool bChangeColorGreen;
    Bool bChangeColorBlue;
    Bool bPaddle1MovedUp;
    Bool bPaddle1MovedDown;
    Bool bPaddle2MovedUp;
    Bool bPaddle2MovedDown;
} Input;

// TODO Game states.
typedef struct {
    Paddle paddle1;
    Paddle paddle2;
    Ball ball;
    Field field;
    Match match;

    TTF_Font* pFont;
    Mix_Chunk* pSoundEffect;

    Event event;
    Input input;
} GameData;

void normalize_vector(Vector2* pVector) {
    double length = sqrt(pVector->x * pVector->x + pVector->y * pVector->y);
    if (length != 0.0) {
        pVector->x /= length;
        pVector->y /= length;
    }
}

// Axis-Aligned Bounding Box (AABB)
Bool check_collision(double x1, double y1, unsigned int width1, unsigned int height1,
                     double x2, double y2, unsigned int width2, unsigned int height2)
{
    return ((x1 < x2 + width2) &&
            (x1 + width1 > x2) &&
            (y1 < y2 + height2) &&
            (y1 + height1 > y2));
}

void reset_ball_position(Ball* pBall, Field field) {
    pBall->ballX = 0.5 * (field.fieldWidth - pBall->ballWidth);
    pBall->ballY = 0.5 * (field.fieldHeight - pBall->ballHeight);
}

void update_paddle(Paddle* pPaddle, double deltaTimeSeconds) {
    normalize_vector(&pPaddle->rectangleMovementDirection);
    pPaddle->rectangleX += pPaddle->rectangleMovementDirection.x * pPaddle->rectangleMovementSpeed * deltaTimeSeconds;
    pPaddle->rectangleY += pPaddle->rectangleMovementDirection.y * pPaddle->rectangleMovementSpeed * deltaTimeSeconds;

    pPaddle->rectangleMovementDirection.x = 0.0;
    pPaddle->rectangleMovementDirection.y = 0.0;
}

Bool check_paddle_ball_collision(Ball ball,
                                 Paddle paddle) {
    return check_collision(ball.ballX, ball.ballY, ball.ballWidth, ball.ballHeight,
                           paddle.rectangleX, paddle.rectangleY, paddle.rectangleWidth, paddle.rectangleHeight);
}

void draw_paddle(SDL_Renderer* pRenderer, Paddle paddle) {
    SDL_Rect rectangle = {};
    rectangle.x = paddle.rectangleX;
    rectangle.y = paddle.rectangleY;
    rectangle.w = paddle.rectangleWidth;
    rectangle.h = paddle.rectangleHeight;
    // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
    SDL_SetRenderDrawColor(pRenderer, paddle.rectangleColorRed, paddle.rectangleColorGreen, paddle.rectangleColorBlue, 255u);
    // <https://wiki.libsdl.org/SDL_RenderDrawRect>
    /* SDL_RenderDrawRect(pRenderer, &rectangle); */
    // <https://wiki.libsdl.org/SDL_RenderFillRect>
    SDL_RenderFillRect(pRenderer, &rectangle);
}

Bool initialize_sdl() {
    // <https://wiki.libsdl.org/SDL_Init>
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        return false;
    }

    // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC8>
    if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF) !=
        (IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF))
    {
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return false;
    }

    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_8.html>
    if (TTF_Init() != 0) {
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return false;
    }

    // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_11.html>
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) == -1) {
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
        TTF_Quit();
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return false;
    }

    return true;
}

Bool create_render_window(const char title[], unsigned int width, unsigned int height,
                          SDL_Window** ppSDLWindow, SDL_Renderer** ppRenderer) {
    // <https://wiki.libsdl.org/SDL_CreateWindow>
    *ppSDLWindow = SDL_CreateWindow(title,
                                    SDL_WINDOWPOS_CENTERED,
                                    SDL_WINDOWPOS_CENTERED,
                                    width,
                                    height,
                                    SDL_WINDOW_SHOWN);
    if (*ppSDLWindow == NULL) {
        // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_12.html#SEC12>
        Mix_CloseAudio();
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
        TTF_Quit();
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return false;
    }

    *ppRenderer = SDL_CreateRenderer(*ppSDLWindow,
                                     -1, // Initialize with the first suitable renderer.
                                     SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (*ppRenderer == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(*ppSDLWindow);
        // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
        IMG_Quit();
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return false;
    }

    return true;
}

void destroy_render_window(SDL_Window** ppSDLWindow, SDL_Renderer** ppRenderer) {
    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyRenderer(*ppRenderer);
    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyWindow(*ppSDLWindow);

    *ppRenderer = NULL;
    *ppSDLWindow = NULL;
}

void destroy_sdl() {
    // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_12.html#SEC12>
    Mix_CloseAudio();
    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_10.html>
    TTF_Quit();
    // <https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html#SEC9>
    IMG_Quit();
    // <https://wiki.libsdl.org/SDL_Quit>
    SDL_Quit();
}

Bool load_game_data(GameData* pData, SDL_Renderer* pRenderer, unsigned int windowWidth, unsigned int windowHeight) {
    pData->field.fieldWidth = windowWidth;
    pData->field.fieldHeight = windowHeight;
    // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
    SDL_SetRenderDrawColor(pRenderer, pData->field.backgroundColorRed, pData->field.backgroundColorGreen, pData->field.backgroundColorBlue, 255u);

    pData->paddle1.rectangleWidth = windowWidth / 40;
    pData->paddle1.rectangleHeight = windowHeight / 6;
    pData->paddle1.rectangleColorRed = 255u;
    pData->paddle1.rectangleColorGreen = 255u;
    pData->paddle1.rectangleColorBlue = 255u;
    pData->paddle1.rectangleX = 0.0;
    pData->paddle1.rectangleY = 0.0;
    // pData->paddle1.rectangleMovementDirection = {};
    pData->paddle1.rectangleMovementSpeed = 400.0;

    pData->paddle2.rectangleWidth = windowWidth / 40;
    pData->paddle2.rectangleHeight = windowHeight / 6;
    pData->paddle2.rectangleColorRed = 255u;
    pData->paddle2.rectangleColorGreen = 255u;
    pData->paddle2.rectangleColorBlue = 255u;
    pData->paddle2.rectangleX = windowWidth - pData->paddle2.rectangleWidth;
    pData->paddle2.rectangleY = windowHeight - pData->paddle2.rectangleHeight;
    // pData->paddle2.rectangleMovementDirection = {};
    pData->paddle2.rectangleMovementSpeed = 400.0;

    // TODO Reference?
    pData->ball.pBallTexture = IMG_LoadTexture(pRenderer, "assets/circle.png");
    if (pData->ball.pBallTexture == NULL) {
        return false;
    }
    // <https://wiki.libsdl.org/SDL_QueryTexture>
    SDL_QueryTexture(pData->ball.pBallTexture, NULL, NULL, (int*) &pData->ball.ballWidth, (int*) &pData->ball.ballHeight);
    pData->ball.ballMovementDirection.x = -1.0;
    pData->ball.ballMovementDirection.y = -1.0;
    normalize_vector(&pData->ball.ballMovementDirection);
    pData->ball.ballMovementSpeed = 250.0;
    reset_ball_position(&pData->ball, pData->field);

    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_14.html>
    pData->pFont = TTF_OpenFont("assets/DejaVuSans.ttf", 48);
    if (pData->pFont == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyTexture>
        SDL_DestroyTexture(pData->ball.pBallTexture);

        return false;
    }

    pData->pSoundEffect = Mix_LoadWAV("assets/blip.wav");
    if (pData->pSoundEffect == NULL) {
        // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_18.html>
        TTF_CloseFont(pData->pFont);
        // <https://wiki.libsdl.org/SDL_DestroyTexture>
        SDL_DestroyTexture(pData->ball.pBallTexture);

        return false;
    }

    return true;
}

void unload_game_data(GameData* pData) {
    // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_24.html>
    Mix_FreeChunk(pData->pSoundEffect);

    // <https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf_18.html>
    TTF_CloseFont(pData->pFont);

    // <https://wiki.libsdl.org/SDL_DestroyTexture>
    SDL_DestroyTexture(pData->ball.pBallTexture);
}

void handle_events(GameData* pData, SDL_Window* pSDLWindow, Bool* pbRunning) {
    // <https://wiki.libsdl.org/SDL_Event>
    SDL_Event event;
    // <https://wiki.libsdl.org/SDL_PollEvent>
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT: {
                *pbRunning = false;

                break;
            }

            case SDL_KEYDOWN:
            case SDL_KEYUP: {
                Bool bPressed = (event.key.type == SDL_KEYDOWN);
                switch (event.key.keysym.sym) {
                    case SDLK_r: {
                        pData->input.bChangeColorRed = bPressed;

                        break;
                    }

                    case SDLK_g: {
                        pData->input.bChangeColorGreen = bPressed;

                        break;
                    }

                    case SDLK_b: {
                        pData->input.bChangeColorBlue = bPressed;

                        break;
                    }

                    case SDLK_w: {
                        pData->input.bPaddle1MovedUp = bPressed;

                        break;
                    }

                    case SDLK_s: {
                        pData->input.bPaddle1MovedDown = bPressed;

                        break;
                    }

                    case SDLK_UP: {
                        pData->input.bPaddle2MovedUp = bPressed;

                        break;
                    }

                    case SDLK_DOWN: {
                        pData->input.bPaddle2MovedDown = bPressed;

                        break;
                    }

                    case SDLK_ESCAPE: {
                        *pbRunning = false;

                        break;
                    }

                    default: {
                        break;
                    }
                }
            }

            default: {
                break;
            }
        }
    }
}

void update_game(GameData* pData, double deltaTimeSeconds) {
    // Process inputs.
    if (pData->input.bChangeColorRed) {
        ++pData->field.backgroundColorRed;
    }
    if (pData->input.bChangeColorGreen) {
        ++pData->field.backgroundColorGreen;
    }
    if (pData->input.bChangeColorBlue) {
        ++pData->field.backgroundColorBlue;
    }

    pData->paddle1.rectangleMovementDirection.y = 0.0;
    if (pData->input.bPaddle1MovedUp) {
        pData->paddle1.rectangleMovementDirection.y -= 1.0;
    }
    if (pData->input.bPaddle1MovedDown) {
        pData->paddle1.rectangleMovementDirection.y += 1.0;
    }

    pData->paddle2.rectangleMovementDirection.y = 0.0;
    if (pData->input.bPaddle2MovedUp) {
        pData->paddle2.rectangleMovementDirection.y -= 1.0;
    }
    if (pData->input.bPaddle2MovedDown) {
        pData->paddle2.rectangleMovementDirection.y += 1.0;
    }

    // Reset events.
    pData->event.bBallFieldCollided = false;
    pData->event.bBallPaddleCollided = false;
    pData->event.bPaddle1Scored = false;
    pData->event.bPaddle2Scored = false;

    // Update logic.
    update_paddle(&pData->paddle1, deltaTimeSeconds);
    update_paddle(&pData->paddle2, deltaTimeSeconds);

    pData->ball.ballX += pData->ball.ballMovementDirection.x * pData->ball.ballMovementSpeed * deltaTimeSeconds;
    if (pData->ball.ballX < 0.0) {
        ++pData->match.scorePaddle2;
        pData->event.bPaddle2Scored = true;
        reset_ball_position(&pData->ball, pData->field);
    } else if (pData->ball.ballX > pData->field.fieldWidth) {
        ++pData->match.scorePaddle1;
        pData->event.bPaddle1Scored = true;
        reset_ball_position(&pData->ball, pData->field);
    }

    pData->ball.ballY += pData->ball.ballMovementDirection.y * pData->ball.ballMovementSpeed * deltaTimeSeconds;
    if (pData->ball.ballY < 0.0) {
        pData->ball.ballY = 0.0;
        pData->ball.ballMovementDirection.y *= -1.0;

        pData->event.bBallFieldCollided = true;
    } else if ((pData->ball.ballY + pData->ball.ballHeight) > pData->field.fieldHeight) {
        pData->ball.ballY = pData->field.fieldHeight - pData->ball.ballHeight;
        pData->ball.ballMovementDirection.y *= -1.0;

        pData->event.bBallFieldCollided = true;
    }

    if (check_paddle_ball_collision(pData->ball, pData->paddle1)) {
        pData->ball.ballMovementDirection.x *= -1;

        pData->event.bBallPaddleCollided = true;
    } else if (check_paddle_ball_collision(pData->ball, pData->paddle2)) {
        pData->ball.ballMovementDirection.x *= -1;

        pData->event.bBallPaddleCollided = true;
    }
}

void render_game(GameData* pData, SDL_Renderer* pRenderer, unsigned int windowWidth, unsigned int windowHeight) {
    // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
    SDL_SetRenderDrawColor(pRenderer, pData->field.backgroundColorRed, pData->field.backgroundColorGreen, pData->field.backgroundColorBlue, 255u);

    // <https://wiki.libsdl.org/SDL_RenderClear>
    SDL_RenderClear(pRenderer);

    draw_paddle(pRenderer, pData->paddle1);
    draw_paddle(pRenderer, pData->paddle2);

    SDL_Rect rectangle = {};
    rectangle.x = pData->ball.ballX;
    rectangle.y = pData->ball.ballY;
    rectangle.w = pData->ball.ballWidth;
    rectangle.h = pData->ball.ballHeight;
    // <https://wiki.libsdl.org/SDL_RenderCopy>
    SDL_RenderCopy(pRenderer, pData->ball.pBallTexture, NULL, &rectangle);

    char text[256];
    sprintf(text, "Placar: %u x %u", pData->match.scorePaddle1, pData->match.scorePaddle2);
    SDL_Color textColor;
    textColor.r = 255;
    textColor.g = 255;
    textColor.b = 255;
    textColor.a = 255;
    // <https://www.libsdl.org/projects/docs/SDL_ttf/SDL_ttf_44.html>
    SDL_Surface* pSurface = TTF_RenderUTF8_Blended_Wrapped(pData->pFont,
                                                           text,
                                                           textColor,
                                                           windowWidth);
    // <https://wiki.libsdl.org/SDL_CreateTextureFromSurface>
    SDL_Texture* pTextTexture = SDL_CreateTextureFromSurface(pRenderer, pSurface);
    // <https://wiki.libsdl.org/SDL_FreeSurface>
    SDL_FreeSurface(pSurface);
    // <https://wiki.libsdl.org/SDL_QueryTexture>
    SDL_QueryTexture(pTextTexture, NULL, NULL, &rectangle.w, &rectangle.h);
    rectangle.x = 0.5 * (windowWidth - rectangle.w);
    rectangle.y = 0;
    // <https://wiki.libsdl.org/SDL_RenderCopy>
    SDL_RenderCopy(pRenderer, pTextTexture, NULL, &rectangle);
    // <https://wiki.libsdl.org/SDL_DestroyTexture>
    SDL_DestroyTexture(pTextTexture);

    // <https://wiki.libsdl.org/SDL_RenderPresent>
    SDL_RenderPresent(pRenderer);
}

void play_sound_effects(GameData* pData) {
    // Process events.
    if (pData->event.bBallFieldCollided) {
        // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_28.html>
        Mix_PlayChannel(-1, pData->pSoundEffect, 0);

    } else if (pData->event.bBallPaddleCollided) {
        // <https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer_28.html>
        Mix_PlayChannel(-1, pData->pSoundEffect, 0);
    } else if (pData->event.bPaddle1Scored || pData->event.bPaddle2Scored) {
        // Do something.
    }
}

int main(int argc, char* argv[]) {
    if (!initialize_sdl()) {
        return EXIT_FAILURE;
    }

    const char title[] = "Framework: Abstrações";
    unsigned int width = 1280u;
    unsigned int height = 720u;
    SDL_Window* pSDLWindow = NULL;
    SDL_Renderer* pRenderer = NULL;
    if (!create_render_window(title, width, height, &pSDLWindow, &pRenderer)) {
        destroy_sdl();

        return EXIT_FAILURE;
    }

    GameData data = {};
    if (!load_game_data(&data, pRenderer, width, height)) {
        destroy_render_window(&pSDLWindow, &pRenderer);
        destroy_sdl();

        return EXIT_FAILURE;
    }

    Uint64 lastTime = 0;
    // <https://wiki.libsdl.org/SDL_GetPerformanceCounter>
    Uint64 currentTime = SDL_GetPerformanceCounter();

    Bool bRunning = true;
    while (bRunning) {
        handle_events(&data, pSDLWindow, &bRunning);

        lastTime = currentTime;
        // <https://wiki.libsdl.org/SDL_GetPerformanceCounter>
        currentTime = SDL_GetPerformanceCounter();
        // <https://wiki.libsdl.org/SDL_GetPerformanceFrequency>
        double deltaTimeSeconds = (currentTime - lastTime) / (double) SDL_GetPerformanceFrequency();
        // double deltaTimeMilliseconds = (currentTime - lastTime) * 1000u / (double) SDL_GetPerformanceFrequency();

        update_game(&data, deltaTimeSeconds);
        play_sound_effects(&data);
        render_game(&data, pRenderer, width, height);

        // <https://wiki.libsdl.org/SDL_Delay>
        // Time in milliseconds.
        SDL_Delay(1000u / 60u);
    }

    unload_game_data(&data);

    destroy_render_window(&pSDLWindow, &pRenderer);
    destroy_sdl();

    return EXIT_SUCCESS;
}
