#include <stdlib.h>

#if defined(__linux__)
#include <SDL2/SDL.h>
#elif defined(_WIN32)
#include "SDL2/SDL.h"
#else
#error Not supported!
#endif

typedef char Bool; // 1 : true, 0 : false
#define true  1
#define false 0

int main(int argc, char* argv[]) {
    // <https://wiki.libsdl.org/SDL_Init>
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        return EXIT_FAILURE;
    }

    const char title[] = "Minha janela movimenta um retângulo";
    unsigned int width = 1280u;
    unsigned int height = 720u;
    SDL_Window* pSDLWindow;
    // <https://wiki.libsdl.org/SDL_CreateWindow>
    pSDLWindow = SDL_CreateWindow(title,
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  width,
                                  height,
                                  SDL_WINDOW_SHOWN);
    if (pSDLWindow == NULL) {
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    SDL_Renderer* pRenderer = SDL_CreateRenderer(pSDLWindow,
                                                 -1, // Initialize with the first suitable renderer.
                                                 SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (pRenderer == NULL) {
        // <https://wiki.libsdl.org/SDL_DestroyWindow>
        SDL_DestroyWindow(pSDLWindow);
        // <https://wiki.libsdl.org/SDL_Quit>
        SDL_Quit();

        return EXIT_FAILURE;
    }

    unsigned int backgroundColorRed = 0u;
    unsigned int backgroundColorGreen = 0u;
    unsigned int backgroundColorBlue = 0u;
    // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
    SDL_SetRenderDrawColor(pRenderer, backgroundColorRed, backgroundColorGreen, backgroundColorBlue, 255u);

    unsigned int rectangleWidth = width / 40;
    unsigned int rectangleHeight = height / 6;
    unsigned int rectangleColorRed = 255u;
    unsigned int rectangleColorGreen = 255u;
    unsigned int rectangleColorBlue = 255u;
    unsigned int rectangleX = 0u;
    unsigned int rectangleY = 0u;

    Bool bRunning = true;
    while (bRunning) {
        // <https://wiki.libsdl.org/SDL_Event>
        SDL_Event event;
        // <https://wiki.libsdl.org/SDL_PollEvent>
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT: {
                    bRunning = false;

                    break;
                }

                case SDL_KEYDOWN:
                case SDL_KEYUP: {
                    Bool bPressed = (event.key.type == SDL_KEYDOWN);
                    if (bPressed) {

                        switch (event.key.keysym.sym) {
                            case SDLK_r: {
                                ++backgroundColorRed;

                                break;
                            }

                            case SDLK_g: {
                                ++backgroundColorGreen;

                                break;
                            }

                            case SDLK_b: {
                                ++backgroundColorBlue;

                                break;
                            }

                            case SDLK_a:
                            case SDLK_LEFT: {
                                --rectangleX;

                                break;
                            }

                            case SDLK_d:
                            case SDLK_RIGHT: {
                                ++rectangleX;

                                break;
                            }

                            case SDLK_w:
                            case SDLK_UP: {
                                --rectangleY;

                                break;
                            }

                            case SDLK_s:
                            case SDLK_DOWN: {
                                ++rectangleY;

                                break;
                            }

                            case SDLK_ESCAPE: {
                                bRunning = false;

                                break;
                            }

                            default: {
                                break;
                            }
                        }
                    }
                }

                default: {
                    break;
                }
            }
        }

        // <https://wiki.libsdl.org/SDL_SetRenderDrawColor>
        SDL_SetRenderDrawColor(pRenderer, backgroundColorRed, backgroundColorGreen, backgroundColorBlue, 255u);

        // <https://wiki.libsdl.org/SDL_RenderClear>
        SDL_RenderClear(pRenderer);

        SDL_Rect rectangle = {};
        rectangle.x = rectangleX;
        rectangle.y = rectangleY;
        rectangle.w = rectangleWidth;
        rectangle.h = rectangleHeight;
        SDL_SetRenderDrawColor(pRenderer, rectangleColorRed, rectangleColorGreen, rectangleColorBlue, 255u);
        // <https://wiki.libsdl.org/SDL_RenderDrawRect>
        /* SDL_RenderDrawRect(pRenderer, &rectangle); */
        // <https://wiki.libsdl.org/SDL_RenderFillRect>
        SDL_RenderFillRect(pRenderer, &rectangle);

        // <https://wiki.libsdl.org/SDL_RenderPresent>
        SDL_RenderPresent(pRenderer);

        // <https://wiki.libsdl.org/SDL_Delay>
        // Time in milliseconds.
        SDL_Delay(1000u / 60u);
    }

    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyRenderer(pRenderer);
    // <https://wiki.libsdl.org/SDL_DestroyWindow>
    SDL_DestroyWindow(pSDLWindow);
    // <https://wiki.libsdl.org/SDL_Quit>
    SDL_Quit();

    return EXIT_SUCCESS;
}
